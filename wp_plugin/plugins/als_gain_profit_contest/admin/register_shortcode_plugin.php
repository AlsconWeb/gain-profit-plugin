<?php

/**
 * Version 1.0.0
 */

add_shortcode('contest', 'register_shortcode');
function register_shortcode($atts)
{
  global $wpdb;
  $table_name = $wpdb->get_blog_prefix() . "gain_profit_contest";
  $content = get_contest($table_name);

  $table = "";
  if ($content) {
    foreach ($content as $val) {
      $new_walle = substr($val->wallet, 0, 4) . '********' . substr($val->wallet, -2);
      $table .= "<tr><td>" . $val->id . "</td><td>" . $new_walle  . "</td></tr>";
    }
  } else {
    $table .= "<tr><td></td><td></td><td></td></tr>";
  }

  $header = $atts["header-text"];
  $text = $atts["title-text"];
  $end_contest = $atts['text-end-contest'];

  $count = get_option('als_count_max');
  $count_now = count($content);
  if ($count_now < $count) {
    $html = '<div class="container-fluid">
            <div class="row">
              <div class="col-xs-12">
                <h1>' . $header . '</h1>
                <form  method="post" class="user-wallet-number">
                  <div class="col-xs-6">
                    <input type="text" class="form-control send-wallet">
                    <input type="hidden" name="person" class="ip-user" value="' . $_SERVER['REMOTE_ADDR'] . '">
                  </div>
                  <div class="col-xs-6">
                    <input type="submit" class="form-control btn btn-primary" value="Отправить" style="padding: 6px 12px;">
                  </div>
                </form>
              </div>
              <div class="row"><div class="col-xs-12"><hr>' . $text . '<hr></div></div>
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Кошелек</th>
                      </tr>
                    </thead>
                    <tbody>
                      ' . $table . '
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>';
  } else {
    $html = '<div class="container-fluid">
            <div class="row">
              <div class="col-xs-12">
                <h1>' . $header . '</h1>
                <p>' . $end_contest . '</p>
              </div>
              <div class="row"><div class="col-xs-12"><hr>' . $text . '<hr></div></div>
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Кошелек</th>
                      </tr>
                    </thead>
                    <tbody>
                      ' . $table . '
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>';
  }
  return $html;
}
