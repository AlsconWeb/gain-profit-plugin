<?php

/**
 * Version 1.0.0
 */

add_action('wp_ajax_add_count_max', 'add_count_max');
add_action('wp_ajax_nopriv_add_count_max', 'add_count_max');

function add_count_max()
{
  $count = $_POST['count'];

  if (get_option('als_count_max')) {
    update_option('als_count_max', $count, 'yes');
  } else {
    add_option('als_count_max', $count, 'yes');
  }


  die(json_encode(array('status' => 'ok', "als_count_max" => $count)));
}

add_action('wp_ajax_count_max', 'count_max');
add_action('wp_ajax_nopriv_count_max', 'count_max');

function count_max()
{

  $count = get_option('als_count_max', 0);

  die(json_encode(array('status' => 'ok', "count" => $count)));
}
