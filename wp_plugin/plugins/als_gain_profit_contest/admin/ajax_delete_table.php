<?php

/**
 * Version 1.0.0
 */

add_action('wp_ajax_delete_table', 'delete_table');
add_action('wp_ajax_nopriv_delete_table', 'delete_table');

function delete_table()
{

  if ($_POST["delete"]) {
    global $wpdb;
    // Remove table from database
    $table_name = $wpdb->get_blog_prefix() . "gain_profit_contest";
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
  }

  die(json_encode(array('status' => 'ok')));
}
